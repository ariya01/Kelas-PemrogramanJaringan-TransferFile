# Transfer-File-Pemrograman-Jaringan

Klien:
- Meminta ke server untuk mengunduh salah satu file pada folder dataset di server (contoh.html, Radiohead - Karma Police.mp3, out.ogv, dan plrabn12.txt) dengan cara mengirim string: "unduh nama_file"
- Menerima isi file dari server
- Melakukan parsing message header. Message header tidak ikut ditulis ke dalam file
- Isi file yang telah diterima dari server disimpan dalam menjadi file tetap sesuai ekstensi filenya.

Server:
- Menerima pesan berupa "unduh nama_file" dari alien
- Membaca file yang diminta oleh klien
- Menambahkan message header yang diletakkan sebelum isi file dengan struktur sebagai berikut (contoh raw message header):
client-id: klien-1,\n
file-size: 2048,\n
\n\n
- Mengirim isi file yang telah dibaca ke klien
- Server dapat menangani banyak klien (gunakan select, thread, dan queue pada Python).
